export interface Config {
    url: string;
    debug?: boolean;
    header?: object;
    method?: string;
    protocols?: string[];
    isReconnect?: boolean;
    reConnectTime?: number;
    isHeartData?: boolean;
    heartTime?: number;
    onSocketOpen?: Function;
    onSocketClose?: Function;
    onSocketError?: Function;
    onSendMessageBefore?: Function;
    onSendMessageAfter?: Function;
    onSocketMessageBefore?: Function;
    onSocketMessageAfter?: Function;
    onNetworkStatusNone?: Function;
}
export interface SendParams {
    event: string;
    data: any;
    extraData?: object;
}
export interface QueueOption {
    uuid: string;
    message: SendParams;
}
export interface Close {
    code?: number;
    reason?: string;
    success?: Function;
    fail?: Function;
    complete?: Function;
}
