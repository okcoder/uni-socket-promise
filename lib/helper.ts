/**
 * 是否是json字符串 如果是直接返回json对象
 * @param str
 */
export const isJson = (str: string | object): boolean | object => {
  if (typeof str === 'object') return str
  try {
    let obj = JSON.parse(str)
    // tslint:disable-next-line:no-extra-boolean-cast
    return !!(typeof obj === 'object' && obj) ? obj : false
  } catch (e) {
    return false
  }
}
