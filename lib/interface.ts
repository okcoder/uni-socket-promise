export interface Config {
  url: string
  debug?: boolean
  header?: object
  method?: string
  protocols?: string[],
  isReconnect?: boolean    // 是否断线重连
  reConnectTime?: number    // 断线重连时间间隔
  isHeartData?: boolean   // 是否开启心跳
  heartTime?: number      // 心跳包间隔事件
  onSocketOpen?: Function
  onSocketClose?: Function
  onSocketError?: Function
  onSendMessageBefore?: Function
  onSendMessageAfter?: Function
  onSocketMessageBefore?: Function    // 收到消息前
  onSocketMessageAfter?: Function     // 收到消息后
  onNetworkStatusNone?: Function      // 监听到没有网络时
}

// 发送消息格式
export interface SendParams {
  event: string,
  data: any,
  extraData?: object
}
// 消息队列
export interface QueueOption {
  uuid: string
  message: SendParams
}

// 被关闭
export interface Close {
  code?: number,
  reason?: string,
  success?: Function
  fail?: Function,
  complete?: Function
}
